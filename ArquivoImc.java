package Imc;

import java.io.File;
import java.util.Scanner;

public class ArquivoImc {
    public static void main(String[]args) {
    	
    	File arquivocsv = new File("C:\\Users\\claudiane\\eclipse-workspace\\Teste\\src\\file\\dataset.csv");
    	
    	try {
    		
    		String linha = new String();
    		
    		Scanner leitor = new Scanner(arquivocsv);
    		
    		leitor.nextLine();
    		
    		while(leitor.hasNext()) {
    		  	
    		   linha = leitor.nextLine();
    		   
    		   String[] valor = linha.split(";");
    		   
    		   double imc;
    		   
    		   valor[2] = valor[2].replaceAll(",", ".");
    		   valor[3] = valor[3].replaceAll(",", ".");
    		   
    		   String nome = valor[0];
    		   String sobrenome = valor[1];
    		   double peso = Double.valueOf(valor[2]).doubleValue();
    		   double altura = Double.valueOf(valor[3]).doubleValue();
    		   
    		   imc = peso / (altura * altura);
    		   
    		   String resultado = String.format("%.2f", imc); 
    		   
    		  System.out.println(nome.toUpperCase() + " " + sobrenome.toUpperCase() + " " + resultado);
    		}
    		
    	} catch (Exception e) {
    		System.out.println("Erro" + e.getMessage());
    	}
    }
}
